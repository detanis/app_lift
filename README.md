# App Lift

App Lift Gadgets is a application which will help you manage you gadgets and found a lot of interesting gadgets shared by other users

## Online

For using online version visit [http://applift.edata.com.ua/](http://applift.edata.com.ua/)

## Installation

### Ruby

AppLift Gadgets requires ruby v2.

### Postgres

You can find instructions for installing postgres [here](http://www.postgresql.org/download/)

## Setup

Clone the repo with AppLift:

```shell
$ git clone https://bitbucket.org/detanis/app_lift.git
```

Install gems:

```shell
$ bundle install
```

Initialize configuration file:

```shell
$ cp .env.sample .env
$ cp config/database.yml.sample config/database.yml
```

Change `.env` and `config/database.yml` files according to you environment


Initialize database:

```shell
$ bundle exec rake db:setup
```

## Run server

It's time to run puma webserver:

```shell
$ bundle exec puma -b tcp://127.0.0.1:3000
```

Now you can access project on `http//127.0.0.1:3000`

## Tests

```shell
$ bundle exec rake rspec
```
