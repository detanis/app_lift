module Features
  module GadgetHelpers
    def overview_value(label_text)
      find(:xpath, "//dt[text()='#{label_text}']/following-sibling::*[1]").text
    end

    def fill_gadget_form(gadget, image_path = Rails.root.join("spec", "factories", "files", "images", "1.jpg"))
      fill_in 'gadget_name', with: gadget.name
      fill_in 'gadget_description', with: gadget.description
      fill_in 'gadget_tags', with: gadget.tags.join(', ')
      attach_file('gadget_image', image_path)
    end

    def gadget_overview(gadget)
      expect(find("h3")).to have_content(gadget.name)
      expect(overview_value('Overview:')).to eq(gadget.description)
      expect(overview_value('Tags:')).to eq(gadget.tags.join(', '))
    end

    def check_list_element(number, gadget)
      item = find("#gadgets .item:nth-of-type(#{number})")
      expect(item.find('.caption h3 a')).to have_content(gadget.name)
      expect(item.find('p.tags')).to have_content(gadget.tags.join(', '))
      item
    end
  end
end
