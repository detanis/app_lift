require 'spec_helper'

feature 'Visitor signs out' do
  scenario 'sign out' do
    sign_in
    click_link 'Sign Out'
    expect(page).to have_content('Sign In')
  end
end
