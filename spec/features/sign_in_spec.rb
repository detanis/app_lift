require 'spec_helper'

feature 'Visitor signs in' do
  before(:each) do
    @user = FactoryGirl.create(:user)
  end

  scenario 'with valid email and password' do
    sign_in_with @user.email, 'qwe'
    expect(page).to have_content('Sign Out')
  end

  scenario 'with incorrect email' do
    sign_in_with 'not_smith@example.com', 'qwe'
    expect(page).to have_content('Bad email or password.')
  end

  scenario 'with incorrect password' do
    sign_in_with @user.email, 'not_qwe'
    expect(page).to have_content('Bad email or password.')
  end

  scenario 'with invalid email' do
    sign_in_with 'invalid_email', 'password'

    expect(page).to have_content('Sign In')
  end

  scenario 'with blank password' do
    sign_in_with @user.email, ''

    expect(page).to have_content('Sign In')
  end

  def sign_in_with(email, password)
    visit sign_in_path
    fill_in 'Email', with: email
    fill_in 'Password', with: password
    click_button 'Sign in'
  end
end
