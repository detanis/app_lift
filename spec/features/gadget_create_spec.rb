require 'spec_helper'

feature 'User creates new gadget' do
  scenario 'valid gadget' do
    sign_in
    gadget = FactoryGirl.build(:gadget, user: @user)

    visit new_gadget_path
    fill_gadget_form(gadget)
    click_button "Create Gadget"
    gadget_overview(gadget)

  end
end