require 'spec_helper'

feature 'Visitor sees gadget information' do
  scenario 'on show page' do
    sign_in
    gadget = FactoryGirl.create(:gadget, user: @user)

    visit gadget_path(gadget)
    gadget_overview(gadget)
  end
end

