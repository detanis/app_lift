require 'spec_helper'

feature 'User see list of gadgets' do
  scenario 'one gadget' do
    sign_in
    gadget = FactoryGirl.create(:gadget, user: @user)

    visit gadgets_path
    check_list_element(1, gadget)
  end

  scenario 'my gadgets' do
    FactoryGirl.create(:updated_gadget)
    sign_in
    gadget = FactoryGirl.create(:gadget, user: @user)

    visit gadgets_path(my: true)

    check_list_element(1, gadget)
  end
end