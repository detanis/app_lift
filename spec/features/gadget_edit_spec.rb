require 'spec_helper'

feature 'User creates new gadget' do
  scenario 'valid gadget' do
    sign_in
    gadget = FactoryGirl.create(:gadget, user: @user)
    updated_gadget = FactoryGirl.build(:updated_gadget, user: @user)

    visit edit_gadget_path(gadget)

    fill_gadget_form(updated_gadget)
    click_button "Update Gadget"
    gadget_overview(updated_gadget)
  end
end