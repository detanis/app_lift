require "spec_helper"

describe User do
  it "create" do
    valid = User.create!(email: "valid@example.com", password: 'password')

    expect(User.first.email).to eq(valid.email)
  end
end
