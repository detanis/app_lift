require "spec_helper"

describe Gadget do
  it "create" do
    gadget = FactoryGirl.build(:gadget)
    gadget.save

    expect(Gadget.first.name).to eq(gadget.name)
  end

  it "handle tags in string representation" do
    gadget = FactoryGirl.build(:gadget, tags: 'TV, Video')
    gadget.save

    expect(Gadget.first.tags).to eq(['TV', 'Video'])
  end


  it "should require a name" do
    FactoryGirl.build(:gadget, name: "").should_not be_valid
  end

  it "should require a decription" do
    FactoryGirl.build(:gadget, description: "").should_not be_valid
  end
  it "should require a user" do
    FactoryGirl.build(:gadget, user: nil).should_not be_valid
  end

  it "should have name not longer than 255" do
    long_name = "a" * 256
    FactoryGirl.build(:gadget, name: long_name).should_not be_valid
  end

  it "should have description not longer than 10000" do
    long_description = "a" * 10001
    FactoryGirl.build(:gadget, description: long_description).should_not be_valid
  end

  it "should filter by only user scope" do
    my_gadget =  FactoryGirl.create(:gadget)
    another_gadget = FactoryGirl.create(:updated_gadget)

    found_gadgets = Gadget.with_user_if_present(my_gadget.user_id)
    expect(found_gadgets).to eq([my_gadget])
  end

  it { should have_attached_file(:image) }
  it { should validate_attachment_presence(:image) }
  it { should validate_attachment_content_type(:image).
                allowing('image/png', 'image/gif', 'image/jpg').
                rejecting('text/plain', 'text/xml') }
  it { should validate_attachment_size(:image).
                less_than(2.megabytes) }


end
