include ActionDispatch::TestProcess

FactoryGirl.define do
  factory :gadget do
    name "iPhone 5"
    description "The iPhone 5 is a touchscreen smartphone developed by Apple Inc. It is the sixth generation of the iPhone, succeeding the iPhone 4S and preceding the iPhone 5S and iPhone 5C. Formally unveiled as part of a press event on September 12, 2012, it was released on September 21, 2012. The iPhone 5 featured major design changes in comparison to its predecessor. These included an aluminum-based body which was thinner and lighter than previous models, a taller screen with a nearly 16:9 aspect ratio, the Apple A6 system-on-chip, LTE support, and Lightning, a new compact dock connector which replaces the 30-pin design used by previous iPhone models."
    tags ['mobile', 'phone']
    association :user
    image { fixture_file_upload(Rails.root.join("spec", "factories", "files", "images", "1.jpg"), "image/jpg") }
  end

  factory :updated_gadget, class: Gadget do
    name "Samsung Galaxy S4"
    description "One of the most impressive things about the phone is the fact the size hasn't changed from its predecessor - the Galaxy S4 comes in at 136.6 x 69.8 x 7.9mm (5.38 x 2.75 x 0.31 inches), meaning there's no extra heft to try to work with in your palm."
    tags ['mobile', 'phone', 'android']
    association :user
    image { fixture_file_upload(Rails.root.join("spec", "factories", "files", "images", "1.jpg"), "image/jpg") }
  end
end
