ActionView::Base.field_error_proc = Proc.new do |html_tag, instance|
  if html_tag =~ /^<label/
    html_tag.html_safe
  else
    errors = Array(instance.error_message).join(',')
    %(<div class='has-error'>#{html_tag}<span class="help_block text-danger">&nbsp;#{errors}</span></div>).html_safe
  end
end