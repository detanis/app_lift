AppLift::Application.routes.draw do
  resources :gadgets

  root 'gadgets#index'
end
