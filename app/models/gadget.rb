class Gadget < ActiveRecord::Base
  belongs_to :user

  validates :name, :description, :user_id, presence: true
  validates :name, length: { maximum: 255 }
  validates :description, length: { maximum: 10000 }

  scope :with_user_if_present, ->(user_id)          { where(user_id: user_id) if user_id.present? }

  has_attached_file :image,
                    styles: { medium: "300x300>", thumb: "100x100>" }

  validates_attachment :image, presence: true,
                       content_type: { content_type: ['image/png', 'image/gif', 'image/jpg', 'image/jpeg'] },
                       size: { in: 0..2.megabytes }

  def tags=(values)
    write_attribute(:tags, (values.is_a?(Array) ? values : values.split(',')).map(&:strip) )
  end
end
