class GadgetsController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_filter :authorize, except: [:index, :show]

  def index
    @gadgets = Gadget.with_user_if_present(params[:my] ? current_user.try(:id) : nil).page(params[:page])
  end

  def show
  end

  def new
    @gadget = current_user.gadgets.build
  end

  def create
    @gadget = current_user.gadgets.build(gadget_params)

    if @gadget.save
      redirect_to @gadget, flash: { success: 'Gadget was successfully created.' }
    else
      render action: 'new'
    end
  end

  def edit
  end

  def update
    if @gadget.update(gadget_params)
      redirect_to @gadget, flash: { success: 'User was successfully updated.' }
    else
      render action: 'edit'
    end
  end

  private

  def set_user
    @gadget = Gadget.find(params[:id])
  end

  def gadget_params
    params.require(:gadget).permit(:name, :description, :image, :tags)
  end
end
