class UsersController < Clearance::UsersController
  private

  def user_from_params
    User.new(params[:user] ? permit_params : Hash.new)
  end

  def permit_params
    params.require(:user).permit(:email, :password)
  end
end
